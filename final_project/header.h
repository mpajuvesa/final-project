#pragma once
#include <string>
#include <Windows.h>

#define HELPCMD "-h"
#define NOREGWRITE "-w"
#define NOREGREAD "-y"
#define WRITETEXT "-t"
#define READTEXT "-r"

#define LANG_EN
#define LANG_FI 1

// REGISTRY NAME
#define REG_NAME "SOFTWARE\\FinalProject"
// REGISTRY KEYS & VALUES
#define ANDREAS_KEY "Andreas"
#define ANDREAS_VALUE "Siivola"
#define MATIAS_KEY "Matias"
#define MATIAS_VALUE "Pajuvesa"
#define TONI_KEY "Toni"
#define TONI_VALUE "Mauno"

#define CUSTOM_KEY "Custom"

// ENG
#ifdef LANG_EN

#define HELP "-h prints help\n-w skips writing to registry\n-y skips reading from registry\n-t skips writing to file\n-r skips reading from file"
#define DESC "Program reads commandline arguments"
#define REG_WRITE_SUCCESS "Registry write successful"
#define REG_WRITE_ERROR "Error setting value for registry key %s\n"
#define REG_QUERY_ERROR "Error reading value for registry key %s\n"
#define REG_KEY_VALUE_PAIRS "Read key-value pairs:\n %s %s\n %s %s\n %s %s\n"
#define REG_CUSTOM "\nNew registry value:"
#define REG_CUSTOM_VALUE_PAIR "Read key-value pair:\n %s %s\n"
#define TEXT_WRITE "\nWriting to file"
#define TEXT_PATH "Enter path:"
#define TEXT_FILENAME "Enter filename:"
#define TEXT_WRITE_DATA "Enter data:"
#define TEXT_WRITE_SUCCESS "Wrote to file"
#define TEXT_WRITE_ERROR "Could not write to file"
#define TEXT_READ "\nReading from file"
#define TEXT_READ_ERROR "Failed to read from file"

#endif // LANG_EN

// FI
#ifdef LANG_FI

#define HELP "-h tulostaa helpin\n-w ohittaa rekisterin kirjoituksen\n-y ohittaa rekisterist� lukemisen\n-t ohittaa tiedostoon kirjoituksen\n-r ohittaa tiedostosta lukemisen"
#define DESC "Ohjelma lukee komentorivi argumenttej� "
#define REG_WRITE_SUCCESS "Rekisteriin kirjoitus onnistui"
#define REG_WRITE_ERROR "Virhe asettaessa rekisteriin arvoa avaimelle %s\n"
#define REG_QUERY_ERROR "Virhe luettaessa reskisteriarvoa avaimelle %s\n"
#define REG_KEY_VALUE_PAIRS "Luetut avain-arvo parit:\n %s %s\n %s %s\n %s %s\n"
#define REG_CUSTOM "\nUuden rekisterin arvo:"
#define REG_CUSTOM_VALUE_PAIR "Luettu avain-arvo pari:\n %s %s\n"
#define TEXT_WRITE "\nKirjoitetaan tiedostoon"
#define TEXT_PATH "Sy�t� polku:"
#define TEXT_FILENAME "Sy�t� tiedoston nimi:"
#define TEXT_WRITE_DATA "Sy�t� data:"
#define TEXT_WRITE_SUCCESS "Kirjoitettiin tekstitiedostoon"
#define TEXT_WRITE_ERROR "Ei voitu kirjoittaa tekstitiedostoon"
#define TEXT_READ "\nLuetaan tiedostosta"
#define TEXT_READ_ERROR "Ei voitu lukea tiedostosta"

#endif // LANG_FI

void help();
void writeReg();
void writeRegCustom();
void readRegCustom();
void writeRegistryValue(std::string value, HKEY hKey);
void writeToFile();
void readFromFile();