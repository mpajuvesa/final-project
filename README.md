# Software Maintenance - Final Work

## Tekijät:
Matias Pajuvesa, Toni Mauno ja Andreas Siivola

Opiskelijanumerot löytyvät repun palautuksesta.

## Toiminta
    Kirjoittaa ja lukee rekisteriin ja vapaavalintaiseen tiedostoon.
    -h komentoriviargumentti tulostaa helpin

## Rekisteri
    Arvot löytyvät HKEY_CURRENT_USER\SOFTWARE\FinalProject alta
