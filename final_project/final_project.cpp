// final_project.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <file.h>

/**
* main
*/
int main(int argc, char* argv[])
{

	bool canWriteReg = true;
	bool canReadReg = true;

	bool canWriteText = true;
	bool canReadText = true;

	std::cout << DESC << std::endl;


	if (argc > 0) {

		for (int i = 1; i < argc; i++) {

			/**
			* -h
			* prints help
			*/
			if (!strcmp(argv[i], HELPCMD)) {
				help();
				return 0;
			}

			/**
			* -w
			* skips writing to reg
			*/
			if (!strcmp(argv[i], NOREGWRITE)) {
				canWriteReg = false;
			}
			/**
			* -y
			* skips reading from reg
			*/
			if (!strcmp(argv[i], NOREGREAD)) {
				canReadReg = false;
			}

			/**
			* -t
			* Skip writing to file
			*/
			if (!strcmp(argv[i], WRITETEXT)) {
				canWriteText = false;
			}

			/**
			* -r
			* Skip reading from file
			*/
			if (!strcmp(argv[i], READTEXT)) {
				canReadText = false;
			}
		}
	}

	if (canWriteReg) {
		writeReg();
		writeRegCustom();
	}

	if (canWriteText) {
		std::cout << TEXT_WRITE << std::endl;
		writeToFile();
	}
	if (canReadText) {
		std::cout << TEXT_READ << std::endl;
		readFromFile();
	}

	if (canReadReg) {
		readRegCustom();
	}

	system("pause");
    return 0;
}

/**
* help
*/
void help() {
	std::cout << HELP << std::endl;
}


/**
* writes initial registry values
* @param hKey
*/
void writeInitialRegValues(HKEY hKey) {
	DWORD ret;
	std::string val1 = ANDREAS_VALUE;
	std::string val2 = MATIAS_VALUE;
	std::string val3 = TONI_VALUE;

	ret = RegSetValueEx(hKey, TEXT(ANDREAS_KEY), NULL, REG_SZ, (const BYTE*)val1.c_str(), (val1.size() + 1) * sizeof(char));
	if (ret != ERROR_SUCCESS) { printf(REG_WRITE_ERROR, ANDREAS_KEY); }
	ret = RegSetValueEx(hKey, TEXT(MATIAS_KEY), NULL, REG_SZ, (const BYTE*)val2.c_str(), (val2.size() + 1) * sizeof(char));
	if (ret != ERROR_SUCCESS) { printf(REG_WRITE_ERROR, MATIAS_KEY); }
	RegSetValueEx(hKey, TEXT(TONI_KEY), NULL, REG_SZ, (const BYTE*)val3.c_str(), (val3.size() + 1) * sizeof(char));
	if (ret != ERROR_SUCCESS) { printf(REG_WRITE_ERROR, TONI_KEY); }
}

/**
* Writes registry value
* @param value
* @param hKey
*/
void writeRegistryValue(std::string value, HKEY hKey) {

	DWORD ret;

	ret = RegSetValueEx(hKey, TEXT(CUSTOM_KEY), NULL, REG_SZ, (const BYTE*)value.c_str(), (value.size() + 1) * sizeof(char));
	if (ret != ERROR_SUCCESS) { printf(REG_WRITE_ERROR, CUSTOM_KEY); }
}

/**
* reads registry values to usable std::wstrings
* @param hKey
* @param key
* @return value
*/
std::wstring readRegValue(HKEY hKey, LPCSTR key) {
	DWORD bufSize = 1024;
	std::wstring szValue(bufSize / sizeof(wchar_t), L'\0');
	DWORD dwType = REG_SZ;
	DWORD dwRet = RegQueryValueEx(hKey, TEXT(key), NULL, &dwType, reinterpret_cast<LPBYTE>(&szValue[0]), &bufSize);
	if (dwRet == ERROR_SUCCESS) { 
		return szValue; 
	}
	else {
		printf(REG_QUERY_ERROR, key);
		return szValue;
	}
}

/**
* print registry values
* @param hKey
*/
void readRegValues(HKEY hKey) {
	std::wstring szValue1 = readRegValue(hKey, ANDREAS_KEY);
	std::wstring szValue2 = readRegValue(hKey, MATIAS_KEY);
	std::wstring szValue3 = readRegValue(hKey, TONI_KEY);

	if (szValue1[0] == L'\0' ||
		szValue2[0] == L'\0' ||
		szValue3[0] == L'\0') {
		return;
	}
	else {
		printf(REG_KEY_VALUE_PAIRS,
			ANDREAS_KEY, szValue1.c_str(),
			MATIAS_KEY, szValue2.c_str(),
			TONI_KEY, szValue3.c_str());
	}
}

/**
* handles all registry stuff
*/
void writeReg() {
	HKEY hKey;
	DWORD dwDisposition;

	RegCreateKeyEx(HKEY_CURRENT_USER, TEXT(REG_NAME), 0, NULL, 0, KEY_ALL_ACCESS, NULL, &hKey, &dwDisposition);

	if (dwDisposition == REG_CREATED_NEW_KEY) {
		writeInitialRegValues(hKey);
	}
	else if (dwDisposition == REG_OPENED_EXISTING_KEY) {
		readRegValues(hKey);
	}

	RegCloseKey(hKey);
	std::cout << REG_WRITE_SUCCESS << std::endl;
}

/**
* writes custom registry
*/
void writeRegCustom() {
	HKEY hKey;
	DWORD dwDisposition;

	// Path
	std::cout << REG_CUSTOM;
	std::string value;
	std::cin >> value;

	RegCreateKeyEx(HKEY_CURRENT_USER, TEXT(REG_NAME), 0, NULL, 0, KEY_ALL_ACCESS, NULL, &hKey, &dwDisposition);

	if (dwDisposition == REG_OPENED_EXISTING_KEY) {
		writeRegistryValue(value, hKey);
	}
}

/**
* Write custom registry
*/
void readRegCustom() {

	HKEY hKey;
	DWORD dwDisposition;
	RegCreateKeyEx(HKEY_CURRENT_USER, TEXT(REG_NAME), 0, NULL, 0, KEY_ALL_ACCESS, NULL, &hKey, &dwDisposition);

	if (dwDisposition == REG_OPENED_EXISTING_KEY) {
		std::wstring szValue = readRegValue(hKey, CUSTOM_KEY);

		if (szValue[0] == L'\0') {
			return;
		}

		printf(REG_CUSTOM_VALUE_PAIR, CUSTOM_KEY, szValue.c_str());
	}
}

/**
* Write to file
*/
void writeToFile() {

	// Path
	std::cout << TEXT_PATH;
	std::string path;
	std::cin >> path;

	// Filename
	std::cout << TEXT_FILENAME;
	std::string filename;
	std::cin >> filename;

	// Data to write
	std::cout << TEXT_WRITE_DATA;
	std::string data;
	std::cin >> data;

	// Write to file
	if (write(data, path, filename)) {
		std::cout << TEXT_WRITE_SUCCESS << std::endl;
	}
	else {
		std::cout << TEXT_WRITE_ERROR << std::endl;
	}
}

/**
* Read from file
*/
void readFromFile() {

	// Path
	std::cout << TEXT_PATH;
	std::string path;
	std::cin >> path;

	// Filename
	std::cout << TEXT_FILENAME;
	std::string filename;
	std::cin >> filename;

	std::string data = read(path, filename);

	if (data.empty()) {
		std::cout << TEXT_READ_ERROR << std::endl;
	}
	else {
		std::cout << data << std::endl;
	}
}