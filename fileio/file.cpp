#include "stdafx.h"
#include "file.h"
#include <fstream>
#include <sstream>

/**
* Write to file
* @param data
* @param path
* @param filename
* @return success
*/
bool write(std::string data, std::string path, std::string filename)
{
	std::wstring wpath (path.begin(), path.end());
	struct stat info;

	stat(path.c_str(), &info);

	if (CreateDirectory(wpath.c_str(), NULL) || info.st_mode & S_IFDIR) {
		std::ofstream myfile;
		myfile.open(path + "\\" + filename);
		myfile << data << std::endl;
		myfile.close();
		return true;
	}

	return false;
}

/*
* Read from file
* @param path
* @param filename
* @return data
*/
std::string read(std::string path, std::string filename)
{
	std::string data;
	std::ifstream myfile(path + "\\" + filename);

	if (myfile.fail()) {
		return std::string();
	};

	if (myfile.is_open()) {
		std::stringstream stream;
		stream << myfile.rdbuf();
		data = stream.str();

		return data;
	}

	return std::string();
}
